package com.kafka.provider.config;

import ch.qos.logback.classic.util.LogbackMDCAdapter;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.common.config.TopicConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaTopicConfig {

    @Bean
    public NewTopic generateTopic(){

        Map<String,String> configurations = new HashMap<>();
        configurations.put(TopicConfig.CLEANUP_POLICY_CONFIG, TopicConfig.CLEANUP_POLICY_DELETE);
        configurations.put(TopicConfig.RETENTION_MS_CONFIG, "86440000"); //despues se borra
        configurations.put(TopicConfig.SEGMENT_BYTES_CONFIG, "1234567891"); //tamaño maximo de cada segmento del topic
        configurations.put(TopicConfig.MAX_MESSAGE_BYTES_CONFIG, "1000012"); //tamaño maximo de cada mensaje

        return TopicBuilder.name("unProgramadorNace-Topic")
                .partitions(2)
                .replicas(2)
                .configs(configurations)
                .build();
    }
}
